/*
 * Copyright (C) 2013, Joao Salada. All rights reserved.
 * Use is subject to license terms.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Joao Salada, personal@joao.salada.eu
 */

/**
 * @brief Simple test/example of CUSTARRAYNIT Macro
 * @file simple_test.c
 * @author Joao Salada <personal@joao.salada.eu>
 * @date December 2013
 * @test simple_test.c Simple test of CUSTARRAYNIT Macro
 * @example simple_test.c
 */
#include <stdio.h>
#include "src/custarraynit.h"

//![code_snippet]

//system one functionalities
#define SYS1_FUNCT1 0
#define SYS1_FUNCT2 1

//system two functionalities
#define SYS2_FUNCT1 0
#define SYS2_FUNCT2 1

//my system supported functionalities
#define MYSYS_SYS1_FUNCT1 0
#define MYSYS_SYS1_FUNCT2 1
#define MYSYS_SYS2_FUNCT1 2
#define MYSYS_SYS2_FUNCT2 3
#define MYSYS_MAXVAL 4



//array initialized with: {SYS1_FUNCT1,SYS1_FUNCT2,SYS2_FUNCT1,SYS2_FUNCT2} <=> {0,1,0,1}
const int array_funct_map[MYSYS_MAXVAL] = {CUSTARRAYNIT(
																						SET_PAIR(MYSYS_SYS1_FUNCT1,SYS1_FUNCT1),
																						SET_PAIR(MYSYS_SYS1_FUNCT2,SYS1_FUNCT2),
																						SET_PAIR(MYSYS_SYS2_FUNCT1,SYS2_FUNCT1),
																						SET_PAIR(MYSYS_SYS2_FUNCT2,SYS2_FUNCT2))};

//array initialized with: {SYS1_FUNCT1,SYS1_FUNCT2,SYS2_FUNCT1,SYS2_FUNCT2} <=> {0,1,0,1}
const int array_funct_map2[MYSYS_MAXVAL] = {CUSTARRAYNIT(
																						SET_PAIR(MYSYS_SYS1_FUNCT2,SYS1_FUNCT2),
																						SET_PAIR(MYSYS_SYS2_FUNCT1,SYS2_FUNCT1),
																						SET_PAIR(MYSYS_SYS2_FUNCT2,SYS2_FUNCT2),
																						SET_PAIR(MYSYS_SYS1_FUNCT1,SYS1_FUNCT1))};


///! [code_snippet]

int main(int argc, char **argv)
{
	int i = 0;
	int expected_array[MYSYS_MAXVAL] = {SYS1_FUNCT1,SYS1_FUNCT2,SYS2_FUNCT1,SYS2_FUNCT2};
	while(i < MYSYS_MAXVAL) {
		if (expected_array[i] != array_funct_map[i] ||
				expected_array[i] != array_funct_map2[i])
			printf("Produced array does not have the expected contents.\n");
		++i;
	}
	printf("Produced array has the expected contents.\n");

	return 0;
}
