/*
 * Copyright (C) 2013, Joao Salada. All rights reserved.
 * Use is subject to license terms.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Joao Salada, personal@joao.salada.eu
 */


/**
 * @file custarraynit.h
 * @author Joao Salada <personal@joao.salada.eu>
 * @date December 2013
 * @brief Implementation of a C Macro that enables custom array initialization
 *
 * @remark This macro is targeted to numerical arrays.
 * @remark Currently supports arrays with maximum size of 10 elements
 *
 */

#ifndef FUNC_CODES_H_
#define FUNC_CODES_H_



/**
 * Assisting functions of \ref GET_I
 * Each function return the ith parameter received.
 * The Macro name convention must be followed, otherwise the \ref GET_I will
 * not be able to use it
 *
 * Naming convention: A'i'(_0,_1,...,_ith,...)_ith
 */

#define A0(_1,...) _1 ///< Returns the first argument received
#define A1(_1,_2,...) _2 ///< Returns the second argument received
#define A2(_1,_2,_3,...) _3 ///< Returns the third argument received
#define A3(_1,_2,_3,_4,...) _4 ///< Returns the forth argument received
#define A4(_1,_2,_3,_4,_5,...) _5 ///< Returns the fifth argument received
#define A5(_1,_2,_3,_4,_5,_6,...) _6 ///< Returns the sixth argument received
#define A6(_1,_2,_3,_4,_5,_6,_7,...) _7 ///< Returns the seventh argument received
#define A7(_1,_2,_3,_4,_5,_6,_7,_8,...) _8 ///< Returns the eighth argument received
#define A8(_1,_2,_3,_4,_5,_6,_7,_8,_9,...) _9 ///< Returns the ninth argument received
#define A9(_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,...) _10 ///< Returns the tenth argument received

/**
 * @brief Returns the ith argument.
 * @remarks i starts a zero
 * @remarks Depends on the defined functions 'A'
 * @remarks Currently supports the retrieving until 10th parameter (i=9)
 */
#define GET_I(i,...) A##i(__VA_ARGS__)

/*
 * Helper Macros that will Build macros of the type Pli(v)
 */

/* Build P*#, *=l #=i */
#define B_PLI(l,i) B_LI(B_PL(l),i) ///< Calls lower macros to form 'Pli': P*#, *=l #=i

/* Builds Pl#, #=i */
#define B_LI(pl,i) B_LI__(pl,i) ///< Assisting Macro of B_LI. Allows arg expansion
#define B_LI__(pl,i) pl##i ///< Concatenates Pl to i

/* Builds P# , #=l */
#define B_PL(l) B_PL__(l) ///< Assisting Macro to B_PL, allowing args expansion
#define B_PL__(l) P##l ///< Concatenates P to l


#define B_PLIV_(x,z) x(z) ///< Final construction step x=Pli; z=v -> Pli(v)
/*----------- */

/**
 * Performs the opposite action of Macro expansion, which I called 'shrink'.
 * Basically, it aggregates the first 10 arguments into one argument which
 * may be input to other macro.
 *
 * @remark currently shrinks 10 arguments
 */
#define SHRINK_V10(_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,...) \
	SHRINK(_1,_2,_3,_4,_5,_6,_7,_8,_9,_10)

#define SHRINK(...) __VA_ARGS__ ///< Phrinks of a list of parameters

#define COMMA_PRINT(...) COMMA_PRINT__(__VA_ARGS__)
#define COMMA_PRINT__(...) ,##__VA_ARGS__

/**
 * Forms and calls the Macro in the form Pli(v). This Macro only returns
 * v, when l=i. All other constructions will return nothing.
 *
 *  @remarks v will be the value of V at index I[i]
 *
 *  @param l i we want to match
 *  @param i i index
 *  @param I list of indexes
 *  @param V list of values indexed by the values of I
 *  @retval empty when i != l
 *  @retval V[I[l]] when i == l =>> V[I[i]]
 */
#define B_PLIV(l,i,V,I) B_PLIV_(BB_BPLI(l,i,I),BB_VARG(i,V))



#define BB_BPLI(l,i,...) B_LI(B_PL(l),GET_I(i,__VA_ARGS__)) ///< creates Pli with values from param list
#define BB_VARG(l,...) GET_I(l,__VA_ARGS__) ///< returns the lth argument input

/**
 * Given a List V and I, returns the value from V, indexed by the value of I,
 * indexed by l.
 *
 * This allows to have random access to a list V whose real indexes values
 * are given by the values present in I
 * @param V list of values to return
 * @param I list of indexes of V
 * @param l the index present in I, whose indexed value in V we want to return
 */
#define MAP_L_TO_V(V,I,l)\
	B_PLIV(l,0,SHRINK_V10(V),SHRINK_V10(I))\
  B_PLIV(l,1,SHRINK_V10(V),SHRINK_V10(I))\
  B_PLIV(l,2,SHRINK_V10(V),SHRINK_V10(I))\
  B_PLIV(l,3,SHRINK_V10(V),SHRINK_V10(I))\
  B_PLIV(l,4,SHRINK_V10(V),SHRINK_V10(I))\
  B_PLIV(l,5,SHRINK_V10(V),SHRINK_V10(I))\
  B_PLIV(l,6,SHRINK_V10(V),SHRINK_V10(I))\
  B_PLIV(l,7,SHRINK_V10(V),SHRINK_V10(I))\
  B_PLIV(l,8,SHRINK_V10(V),SHRINK_V10(I))\
  B_PLIV(l,9,SHRINK_V10(V),SHRINK_V10(I))

/*
 * These Macros allow the user to input less arguments than the maximum allowed
 * without any error. They basically fill the gaps
 */
#define ARRAY_NULL_10 n,n,n,n,n,n,n,n,n,n,n ///< creates a list of 10 parameters equal to n
#define ARRAY_NULL_20 ARRAY_NULL_10,ARRAY_NULL_10 ///< creates a list of 20 parameter equal to n

/**
 * Given 10 pair of index+value, returns a list with ony the v
 *
 * @remark will only return the first 10 vs
 */
#define ARRAY_GET_V(_i0,_v0,_i1,_v1,_i2,_v2,_i3,_v3,_i4,_v4,_i5,_v5,_i6,_v6,\
										_i7,_v7,_i8,_v8,_i9,_v9,...)\
										_v0,_v1,_v2,_v3,_v4,_v5,_v6,_v7,_v8,_v9

/**
 * Given 10 pair of index+value, returns a list with only the i
 *
 * @remark will only return the first 10 is
 */
#define ARRAY_GET_I(_i0,_v0,_i1,_v1,_i2,_v2,_i3,_v3,_i4,_v4,_i5,_v5,_i6,_v6,\
										_i7,_v7,_i8,_v8,_i9,_v9,...)\
										_i0,_i1,_i2,_i3,_i4,_i5,_i6,_i7,_i8,_i9

/**
 * Given a list of indexes I and a list of the respective Values V,
 * return a list with sorted values by index present in I.
 *
 * @remark Will only work with the first 10 V values. I values must not be greater than 9.
 * @remark The index present at position i of I, will correspond to the value in V
 * at the same position i
 * @remark Values v are returned in sorted order by the value of i in I.
 * @retval V[I[i=0]], V[I[i]], ...
 *
 */
#define INIT_ARRAY(I,V) \
	MAP_L_TO_V(SHRINK_V10(V),SHRINK_V10(I),0) \
	COMMA_PRINT(MAP_L_TO_V(SHRINK_V10(V),SHRINK_V10(I),1)) \
	COMMA_PRINT(MAP_L_TO_V(SHRINK_V10(V),SHRINK_V10(I),2)) \
	COMMA_PRINT(MAP_L_TO_V(SHRINK_V10(V),SHRINK_V10(I),3)) \
	COMMA_PRINT(MAP_L_TO_V(SHRINK_V10(V),SHRINK_V10(I),4)) \
	COMMA_PRINT(MAP_L_TO_V(SHRINK_V10(V),SHRINK_V10(I),5)) \
	COMMA_PRINT(MAP_L_TO_V(SHRINK_V10(V),SHRINK_V10(I),6)) \
 	COMMA_PRINT(MAP_L_TO_V(SHRINK_V10(V),SHRINK_V10(I),7)) \
	COMMA_PRINT(MAP_L_TO_V(SHRINK_V10(V),SHRINK_V10(I),8)) \
	MAP_L_TO_V(SHRINK_V10(V),SHRINK_V10(I),9)


/**
 * API: Accepts Pairs of [i,v] and outputs v separated by commas and sorted
 * by the corresponding i value.
 *
 * @remark i values must not be repeated
 * @remark skipped indexes will have non-initialized content
 * @remark currently supports 10 pairs at most
 *
 */
#define CUSTARRAYNIT(...) CUSTARRAYNIT_(__VA_ARGS__)

#define CUSTARRAYNIT_(...) CUSTARRAYNIT__(__VA_ARGS__,ARRAY_NULL_20)

/**
 * SET_ARRAY_PAIRS implementation
 */
#define CUSTARRAYNIT__(...) \
	INIT_ARRAY(ARRAY_GET_I(__VA_ARGS__),ARRAY_GET_V(__VA_ARGS__))


/**
 * API: Inserts one pair of the array
 * @param i index where to insert value in array
 * @param v value to insert in array
 */
#define SET_PAIR(i,v) i,v


/*
 * Assisting MACROS of the form Pli(v), where only when i==l, the input value
 * will be returned
 */

#define P00(v) v
#define P01(v)
#define P02(v)
#define P03(v)
#define P04(v)
#define P05(v)
#define P06(v)
#define P07(v)
#define P08(v)
#define P09(v)
#define P0n(v)

#define P10(v)
#define P11(v) v
#define P12(v)
#define P13(v)
#define P14(v)
#define P15(v)
#define P16(v)
#define P17(v)
#define P18(v)
#define P19(v)
#define P1n(v)

#define P20(v)
#define P21(v)
#define P22(v) v
#define P23(v)
#define P24(v)
#define P25(v)
#define P26(v)
#define P27(v)
#define P28(v)
#define P29(v)
#define P2n(v)

#define P30(v)
#define P31(v)
#define P32(v)
#define P33(v) v
#define P34(v)
#define P35(v)
#define P36(v)
#define P37(v)
#define P38(v)
#define P39(v)
#define P3n(v)

#define P40(v)
#define P41(v)
#define P42(v)
#define P43(v)
#define P44(v) v
#define P45(v)
#define P46(v)
#define P47(v)
#define P48(v)
#define P49(v)
#define P4n(v)

#define P50(v)
#define P51(v)
#define P52(v)
#define P53(v)
#define P54(v)
#define P55(v) v
#define P56(v)
#define P57(v)
#define P58(v)
#define P59(v)
#define P5n(v)

#define P60(v)
#define P61(v)
#define P62(v)
#define P63(v)
#define P64(v)
#define P65(v)
#define P66(v) v
#define P67(v)
#define P68(v)
#define P69(v)
#define P6n(v)


#define P70(v)
#define P71(v)
#define P72(v)
#define P73(v)
#define P74(v)
#define P75(v)
#define P76(v)
#define P77(v) v
#define P78(v)
#define P79(v)
#define P7n(v)

#define P80(v)
#define P81(v)
#define P82(v)
#define P83(v)
#define P84(v)
#define P85(v)
#define P86(v)
#define P87(v)
#define P88(v) v
#define P89(v)
#define P8n(v)

#define P90(v)
#define P91(v)
#define P92(v)
#define P93(v)
#define P94(v)
#define P95(v)
#define P96(v)
#define P97(v)
#define P98(v)
#define P99(v) v
#define P9n(v)


#endif /* FUNC_CODES_H_ */
